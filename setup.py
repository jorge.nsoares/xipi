#!/usr/bin/env python3
from setuptools import setup, find_packages
from distutils.util import convert_path
import os

main_ns = {}
ver_path = convert_path('web/version.py')
with open(ver_path) as ver_file:
    exec(ver_file.read(), main_ns)

setup(
    name='xiaopi',
    description='xiaomi rpi automation tool',
    version=main_ns['__version__'],
    url='https://github.com/jorgensoares/xiaopi',
    author='Jorge Soares',
    packages=find_packages(exclude=['tests']),
    install_requires=['flask',
                      'apscheduler',
                      'flask_sqlalchemy',
                      'pymysql',
                      'flask-login',
                      'flask-restful',
                      'Flask-HTTPAuth',
                      'flask-moment',
                      'requests',
                      'werkzeug',
                      'itsdangerous',
                      'Flask-Mail',
                      'Flask-WTF',
                      'Flask-Principal',
                      'wtforms',
                      'Flask-APScheduler',
                      'pyyaml',
                      'pytz',
                      'python-dateutil',
                      'click==6.7',
                      'Jinja2',
                      'MarkupSafe',
                      'miflora',
                      'bluepy',
                      'btlewrap'
                      ],
    include_package_data=True,
    zip_safe=False,
)


if not os.path.exists('/var/log/xiaopi'):
    os.makedirs('/var/log/xiaopi')
