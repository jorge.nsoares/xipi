Introduction
============

**Xiaopi** 

Raspberry pi automation with Xiaomi sensors


## Documentation & Installation Guide

Run the following commands

```commandline
sudo apt-get install python3 python3-pip python3-dev python3-rpi.gpio bluez git
cd /opt
git clone https://github.com/jorgensoares/xiaopi
cd xiaopi
sudo python3 setup.py install
cp scripts/init/raspbian/xiaopi /etc/init.d/
sudo update-rc.d xiaopi defaults
cd /opt
git clone git://git.drogon.net/wiringPi
cd wiringPi
./build
```

Update the settings in `config.py`
```commandline
nano config.py
```

Scan the bluetooth devices with
```commandline
sudo hcitool lescan
```

Update the sensors configuration file `sensors.yaml`
```commandline
nano sensors.yaml
```

Start the application
```commandline
service xiaopi start
```

Go to `http://rpi_ip:8081` and login with `admin` and password `zaq12wsx`

You should change the admin password right after the first login.


### Contribution
Jorge Soares - jorge.nsoares@gmail.com

### License

MIT

