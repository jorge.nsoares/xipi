from flask import render_template, Response, Blueprint, current_app
from flask_login import login_required
import config
if config.PI_CAMERA is True:
    from web.cameras.camera_pi import Camera


mod_cameras = Blueprint('cameras', __name__, url_prefix='/cameras')


def gen(camera):
    """Video streaming generator function."""
    while True:
        frame = camera.get_frame()
        yield (b'--frame\r\n'
               b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n')


@mod_cameras.route('/feed')
@login_required
def camera():
    return render_template('cameras/feed.html', current_app=current_app)


@mod_cameras.route('/video_feed')
@login_required
def video_feed():
    """Video streaming route. Put this in the src attribute of an img tag."""
    return Response(gen(Camera()),
                    mimetype='multipart/x-mixed-replace; boundary=frame')

