from datetime import datetime, timedelta
import sys
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from jinja2.loaders import PackageLoader
from jinja2 import Environment
import config
import os
import yaml

ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg', 'gif'])


def get_previous_date(days):
    return datetime.utcnow() - timedelta(days=days)


def get_now():
    # get the current date and time as a string
    return datetime.utcnow()


def sigterm_handler(_signo, _stack_frame):
    # When sysvinit sends the TERM signal, cleanup before exiting.
    print("received signal {}, exiting...".format(_signo))
    sys.exit(0)


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


def database_link(db_type, database, host=None, user=None, password=None, port=None):
    if db_type == 'sqlite':
        return db_type + ':///' + database
    elif db_type == 'mysql':
        if port:
            return db_type + '://' + user + ':' + password + '@' + host + ':' + port + '/' + database
        else:
            return db_type + '://' + user + ':' + password + '@' + host + '/' + database
    elif db_type == 'postgresql':
        if port:
            return db_type + '://' + user + ':' + password + '@' + host + ':' + port + '/' + database
        else:
            return db_type + '://' + user + ':' + password + '@' + host + '/' + database
    else:
        return None


def authenticate_scheduler_api(auth):
    return auth['username'] == 'test' and auth['password'] == 'test'


def failed_job_listener(event):
    if event.exception:
        subject = "background task error"
        j2_env = Environment(loader=PackageLoader(__name__), trim_blocks=True)
        html_body = j2_env.get_template('error_mail.html').render(
            timestamp=event.scheduled_run_time,
            job_id=event.job_id,
            exception=event.exception,
            traceback=event.traceback
        )
        msg = MIMEMultipart('alternative')
        msg['Subject'] = subject
        msg['From'] = config.MAIL_DEFAULT_SENDER
        msg['To'] = config.ADMIN_EMAIL
        body = MIMEText(html_body, 'html')
        msg.attach(body)
        server = smtplib.SMTP('{}:{}'.format(config.MAIL_SERVER, config.MAIL_PORT))
        server.ehlo()
        server.starttls()
        server.login(config.MAIL_USERNAME, config.MAIL_PASSWORD)
        server.sendmail(config.MAIL_DEFAULT_SENDER, config.ADMIN_EMAIL,  msg.as_string())
        server.quit()


def get_sensors():
    yaml_file = os.path.join(os.path.dirname(os.path.dirname(__file__)), 'sensors.yml')
    with open(yaml_file, 'r') as stream:
        sensors_data = yaml.load(stream)
    return sensors_data


def get_relays():
    yaml_file = os.path.join(os.path.dirname(os.path.dirname(__file__)), 'relays.yml')
    with open(yaml_file, 'r') as stream:
        sensors_data = yaml.load(stream)
    return sensors_data
