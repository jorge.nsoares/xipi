from flask import render_template, Blueprint, current_app
from flask_login import login_required, fresh_login_required
from web.functions import get_previous_date, get_now, get_sensors
from web.relays.models import Schedules
from web.sensors.models import TempHumRead, Alerts, Mijia, Flora, FloraRead
from web.roles import admin_permission


mod_application = Blueprint('application', __name__, url_prefix='/application')


@mod_application.route("/dashboard")
@login_required
def dashboard():
    try:
        mijia_sensor_data = Mijia.query.first().readings.filter(
            TempHumRead.timestamp.between(get_previous_date(1), get_now())).order_by(TempHumRead.timestamp.asc())

    except AttributeError:
        mijia_sensor_data = list()

    try:
        last_reading_mijia = mijia_sensor_data[-1]
    except IndexError:
        last_reading_mijia = dict()

    try:
        flora_sensor_data = Flora.query.first().readings.filter(
            FloraRead.timestamp.between(get_previous_date(1), get_now())).order_by(FloraRead.timestamp.asc()).all()
    except AttributeError:
        flora_sensor_data = list()

    try:
        last_reading_flora = flora_sensor_data[-1]
    except IndexError:
        last_reading_flora = dict()

    return render_template('application/dashboard.html', status=dict(), current_app=current_app,
                           mijia_sensor_data=mijia_sensor_data, flora_sensor_data=flora_sensor_data,
                           schedules=Schedules.query.filter(Schedules.enabled == 'true').all(),
                           alerts=Alerts.query.filter(Alerts.status == 'open').all(),
                           last_reading_mijia=last_reading_mijia,
                           last_reading_flora=last_reading_flora
                           )


@mod_application.route("/settings", methods=['GET'])
@admin_permission.require(http_exception=403)
@fresh_login_required
def settings():
    return render_template('application/settings.html', current_app=current_app, mijia_sensors=Mijia.query.all(),
                           flora_sensors=Flora.query.all(), relays=[], cameras=[])
