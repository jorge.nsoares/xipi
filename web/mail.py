from flask_mail import Mail
import logging
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email import encoders
import os
import smtplib
import config

LOGGER = logging.getLogger(__name__)

# Flask mail
mail = Mail()


class MailClient(object):
    def __init__(self, host, port, username, password):
        self._host = host
        self._port = port
        self._username = username
        self._password = password
        self._connection = None

    def connect(self):
        self._connection = smtplib.SMTP(self._host, self._port)
        self._connection.ehlo()
        self._connection.starttls()
        self._connection.login(self._username, self._password)

    def __enter__(self):
        self.connect()

    def __exit__(self, exc_type, exc_val, exc_tb):
        self._connection.close()

    def send(self, recipients, subject, html_body, attachments=list()):

        # Create message container - the correct MIME type is multipart/alternative.
        msg = MIMEMultipart('alternative')
        msg['Subject'] = subject
        msg['From'] = config.MAIL_DEFAULT_SENDER
        msg['To'] = recipients

        body = MIMEText(html_body, 'html')
        msg.attach(body)

        # Add attachments
        for f in attachments:
            part = MIMEBase('application', "octet-stream")
            part.set_payload(open(f, "rb").read())
            encoders.encode_base64(part)
            part.add_header('Content-Disposition', 'attachment; filename="{0}"'.format(os.path.basename(f)))
            msg.attach(part)

        self._connection.sendmail(config.MAIL_DEFAULT_SENDER, [recipient.strip() for recipient in recipients.split(',')],
                                  msg.as_string())


