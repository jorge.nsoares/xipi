from flask_apscheduler import APScheduler


scheduler = APScheduler()

JOBS = [
    {
        'id': '1',
        'name': 'Read Sensors',
        'func': 'web.sensors.tasks:read_sensors',
        'timezone': 'utc',
        'trigger': 'interval',
        'seconds': 300
    }
]
