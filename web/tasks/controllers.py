import requests
from requests.auth import HTTPBasicAuth
from flask import Blueprint, render_template, request, current_app, jsonify
from flask_login import login_required
from web.roles import admin_permission
from dateutil import parser
from web.scheduler import scheduler

mod_tasks = Blueprint('tasks', __name__, url_prefix='/tasks')


@mod_tasks.route("/list", methods=['GET'])
@login_required
@admin_permission.require(http_exception=403)
def background_tasks():
    scheduler_info = requests.get('{0}scheduler'.format(request.url_root), auth=HTTPBasicAuth('test', 'test'),
                                  timeout=1).json()
    jobs = requests.get('{0}scheduler/jobs'.format(request.url_root), auth=HTTPBasicAuth('test', 'test'),
                        timeout=1).json()
    for job in jobs:
        try:
            job['start_date'] = parser.parse(job['start_date'])
        except KeyError:
            pass
        job['next_run_time'] = parser.parse(job['next_run_time'])
        if job['trigger'] == 'cron':
            job['time'] = parser.parse('{}:{}+00:00'.format(job['hour'], job['minute']))

    return render_template('tasks/list.html', current_app=current_app, tasks=jobs, scheduler=scheduler_info)
