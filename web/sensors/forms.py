from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms.validators import DataRequired

from web.sensors.models import Mijia


class CreateMijiaSensor(FlaskForm):
    name = StringField('relay', validators=[DataRequired()])
    address = StringField('address', validators=[DataRequired()])
    temperature_operator = StringField('temperature_operator')
    temperature_threshold = StringField('temperature_threshold')
    temperature_status = StringField('temperature_status', validators=[DataRequired()])
    humidity_operator = StringField('humidity_operator')
    humidity_threshold = StringField('humidity_threshold')
    humidity_status = StringField('humidity_status', validators=[DataRequired()])
    battery_operator = StringField('battery_operator')
    battery_threshold = StringField('battery_threshold')
    battery_status = StringField('battery_status', validators=[DataRequired()])

    def validate(self):
        rv = FlaskForm.validate(self)
        if not rv:
            return False

        sensors = Mijia.query.all()
        for sensor in sensors:
            if sensor.name == self.name.data:
                self.name.errors.\
                    append('A sensor with the chosen name already exist, please use a different name.')
                return False

        if self.temperature_status.data == 'true':
            if not self.temperature_threshold.data:
                self.temperature_threshold.errors.\
                    append('You need to specify a temperature threshold if you want to enable the temperature check')
                return False

        if self.humidity_status.data == 'true':
            if not self.humidity_threshold.data:
                self.humidity_threshold.errors.\
                    append('You need to specify a humidity threshold if you want to enable the humidity check')
                return False

        if self.battery_status.data == 'true':
            if not self.battery_threshold.data:
                self.battery_threshold.errors.\
                    append('You need to specify a humidity threshold if you want to enable the humidity check')
                return False

        return True


class CreateFloraSensor(FlaskForm):
    name = StringField('Sensor name', validators=[DataRequired()])
    address = StringField('Sensor address', validators=[DataRequired()])
    temperature_operator = StringField('temperature_operator')
    temperature_threshold = StringField('temperature_threshold')
    temperature_status = StringField('temperature_status', validators=[DataRequired()])
    moisture_operator = StringField('moisture_operator')
    moisture_threshold = StringField('moisture_threshold')
    moisture_status = StringField('moisture_status', validators=[DataRequired()])
    light_operator = StringField('light_operator')
    light_threshold = StringField('light_threshold')
    light_status = StringField('light_status', validators=[DataRequired()])
    conductivity_operator = StringField('conductivity_operator')
    conductivity_threshold = StringField('conductivity_threshold')
    conductivity_status = StringField('conductivity_status', validators=[DataRequired()])
    battery_operator = StringField('battery_operator')
    battery_threshold = StringField('battery_threshold')
    battery_status = StringField('battery_status', validators=[DataRequired()])

    def validate(self):
        rv = FlaskForm.validate(self)
        if not rv:
            return False

        sensors = Mijia.query.all()
        for sensor in sensors:
            if sensor.name == self.name.data:
                self.name.errors.\
                    append('A sensor with the chosen name already exist, please use a different name.')
                return False

        if self.temperature_status.data == 'true':
            if not self.temperature_threshold.data:
                self.temperature_threshold.errors.\
                    append('You need to specify a temperature threshold if you want to enable the temperature check')
                return False

        if self.moisture_status.data == 'true':
            if not self.moisture_threshold.data:
                self.moisture_threshold.errors.\
                    append('You need to specify a moisture threshold if you want to enable the moisture check')
                return False

        if self.light_status.data == 'true':
            if not self.light_threshold.data:
                self.light_threshold.errors.\
                    append('You need to specify a light threshold if you want to enable the light check')
                return False

        if self.conductivity_status.data == 'true':
            if not self.conductivity_threshold.data:
                self.conductivity_threshold.errors.\
                    append('You need to specify a conductivity threshold if you want to enable the conductivity check')
                return False

        if self.battery_status.data == 'true':
            if not self.battery_threshold.data:
                self.battery_threshold.errors.\
                    append('You need to specify a humidity threshold if you want to enable the humidity check')
                return False

        return True
