from web.sensors.checks import CheckAlert
from web.sensors.mijia import MijiaPoller, MI_HUMIDITY, MI_TEMPERATURE, MI_BATTERY
from web.sensors.models import TempHumRead, Mijia, Checks, Flora, FloraRead
from web.database import db
from web.manager.models import User
import logging
from btlewrap import BluepyBackend
from miflora.miflora_poller import MiFloraPoller, MI_CONDUCTIVITY, MI_MOISTURE, MI_LIGHT, MI_TEMPERATURE, MI_BATTERY

LOGGER = logging.getLogger(__name__)


def read_sensors():
    with db.app.app_context():
        mijia_sensors = Mijia.query.all()
        flora_sensors = Flora.query.all()
        observers = User.query.filter(
            (User.pushbullet_alert is True) | (User.sms_alert is True) | (User.email_alert is True)).all()

        for sensor in flora_sensors:
            checks = Checks.query.filter(Checks.sensor_name == sensor.name).all()

            poller = MiFloraPoller(sensor.address, BluepyBackend)
            sensor_name = poller.name()
            firmware = poller.firmware_version()
            temperature = poller.parameter_value(MI_TEMPERATURE)
            moisture = poller.parameter_value(MI_MOISTURE)
            light = poller.parameter_value(MI_LIGHT)
            conductivity = poller.parameter_value(MI_CONDUCTIVITY)
            battery = poller.parameter_value(MI_BATTERY)

            db.session.add(FloraRead(sensor, temperature, moisture, light, conductivity, battery, sensor.address,
                                     sensor_name, firmware))
            db.session.commit()
            LOGGER.info('Sensor: {}, Temperature: {}°C, Moisture: {}%, Light: {} Lux, Conductivity: {} µS/cm, '
                        'Battery: {}%, Model: {}, Firmware: {}'.format(sensor.name, temperature, moisture, light,
                                                                       conductivity, battery, sensor_name, firmware))
            for check in checks:
                if check.metric == 'temperature':
                    CheckAlert('temperature', sensor, check.threshold, temperature, check.operator, check.type,
                                  observers).execute()
                if check.metric == 'moisture':
                    CheckAlert('moisture', sensor, check.threshold, moisture, check.operator, check.type,
                                  observers).execute()
                if check.metric == 'light':
                    CheckAlert('light', sensor, check.threshold, light, check.operator, check.type,
                                  observers).execute()
                if check.metric == 'conductivity':
                    CheckAlert('conductivity', sensor, check.threshold, conductivity, check.operator, check.type,
                                  observers).execute()
                if check.metric == 'battery':
                    CheckAlert('battery', sensor, check.threshold, battery, check.operator, check.type,
                                  observers).execute()

        for sensor in mijia_sensors:
            checks = Checks.query.filter(Checks.sensor_name == sensor.name).all()

            poller = MijiaPoller(sensor.address)
            sensor_name = poller.name()
            firmware = poller.firmware_version()
            temperature = poller.parameter_value(MI_TEMPERATURE)
            humidity = poller.parameter_value(MI_HUMIDITY)
            battery = poller.parameter_value(MI_BATTERY)

            db.session.add(TempHumRead(sensor, temperature, humidity, battery, sensor.address, sensor_name, firmware))
            db.session.commit()
            LOGGER.info('Sensor: {}, Temperature: {}°C, Humidity: {}%, Battery: {}%, Model: {}, Firmware: {}'.
                        format(sensor.name, temperature, humidity, battery, sensor_name, firmware))

            for check in checks:
                if check.metric == 'temperature':
                    CheckAlert('temperature', sensor, check.threshold, temperature, check.operator, check.type,
                                  observers).execute()
                if check.metric == 'humidity':
                    CheckAlert('humidity', sensor, check.threshold, humidity, check.operator, check.type,
                                  observers).execute()
                if check.metric == 'battery':
                    CheckAlert('battery', sensor, check.threshold, battery, check.operator, check.type,
                                  observers).execute()
