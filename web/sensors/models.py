from web.database import db
from web.functions import get_now


class Mijia(db.Model):
    __tablename__ = 'sensors.mijia'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), nullable=False, unique=True, index=True)
    address = db.Column(db.String(100), nullable=False, index=True)
    temperature = db.Column(db.Float)
    humidity = db.Column(db.Float)
    battery = db.Column(db.Float)
    model = db.Column(db.String(100), index=True)
    firmware = db.Column(db.String(100))
    status = db.Column(db.String(5))
    last_check = db.Column(db.DateTime)

    def __init__(self, name, address):
        self.name = name
        self.address = address


class TempHumRead(db.Model):
    __tablename__ = 'sensors.temp_hum_read'

    id = db.Column(db.Integer, primary_key=True)
    timestamp = db.Column(db.DateTime, nullable=False, index=True)
    sensor_id = db.Column(db.Integer, db.ForeignKey('sensors.mijia.id'), nullable=False)
    sensor = db.relationship('Mijia', backref=db.backref('readings', lazy='dynamic'))
    temperature = db.Column(db.Float)
    humidity = db.Column(db.Float)
    battery = db.Column(db.Float)
    address = db.Column(db.String(100), nullable=False, index=True)
    model = db.Column(db.String(100), nullable=False, index=True)
    firmware = db.Column(db.String(100))

    def __init__(self, sensor, temperature, humidity, battery, address, model, firmware):
        self.timestamp = get_now()
        self.sensor = sensor
        self.temperature = temperature
        self.humidity = humidity
        self.battery = battery
        self.address = address
        self.model = model
        self.firmware = firmware


class Flora(db.Model):
    __tablename__ = 'sensors.flora'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), nullable=False, unique=True, index=True)
    address = db.Column(db.String(100), nullable=False, index=True)
    temperature = db.Column(db.Float)
    humidity = db.Column(db.Float)
    battery = db.Column(db.Float)
    model = db.Column(db.String(100), index=True)
    firmware = db.Column(db.String(100))
    status = db.Column(db.String(5))
    last_check = db.Column(db.DateTime)

    def __init__(self, name, address):
        self.name = name
        self.address = address


class FloraRead(db.Model):
    __tablename__ = 'sensors.flora_read'

    id = db.Column(db.Integer, primary_key=True)
    timestamp = db.Column(db.DateTime, nullable=False, index=True)
    sensor_id = db.Column(db.Integer, db.ForeignKey('sensors.flora.id'), nullable=False)
    sensor = db.relationship('Flora', backref=db.backref('readings', lazy='dynamic'))
    temperature = db.Column(db.Float)
    moisture = db.Column(db.Integer)
    light = db.Column(db.Integer)
    conductivity = db.Column(db.Integer)
    battery = db.Column(db.Integer)
    address = db.Column(db.String(100), nullable=False, index=True)
    model = db.Column(db.String(100), nullable=False, index=True)
    firmware = db.Column(db.String(100))

    def __init__(self, sensor, temperature, moisture, light, conductivity, battery, address, model, firmware):
        self.timestamp = get_now()
        self.sensor = sensor
        self.temperature = temperature
        self.moisture = moisture
        self.light = light
        self.conductivity = conductivity
        self.battery = battery
        self.address = address
        self.model = model
        self.firmware = firmware


class Alerts(db.Model):

    __tablename__ = 'sensors.alerts'

    id = db.Column(db.Integer, primary_key=True)
    timestamp = db.Column(db.DateTime, index=True)
    sensor_id = db.Column(db.Integer, db.ForeignKey('sensors.mijia.id'), nullable=False)
    sensor = db.relationship('Mijia', backref=db.backref('alerts', lazy='dynamic'))
    metric = db.Column(db.String(100), nullable=False, index=True)
    threshold = db.Column(db.Float)
    reading = db.Column(db.Float)
    operator = db.Column(db.String(4))
    status = db.Column(db.String(10))
    check_id = db.Column(db.String(8))
    last_check = db.Column(db.DateTime)
    closed_on = db.Column(db.DateTime)
    closed_by = db.Column(db.String(80), index=True)

    def __init__(self, sensor_id, metric, threshold, reading, operator, status, check_id='default'):

        self.timestamp = get_now()
        self.sensor_id = sensor_id
        self.metric = metric
        self.threshold = threshold
        self.reading = reading
        self.operator = operator
        self.status = status
        self.check_id = check_id


class Checks(db.Model):

    __tablename__ = 'sensors.checks'

    id = db.Column(db.Integer, primary_key=True)
    sensor_name = db.Column(db.String(255), nullable=False, index=True)
    metric = db.Column(db.String(255), nullable=False, index=True)
    threshold = db.Column(db.Float, nullable=False)
    operator = db.Column(db.String(2), nullable=False)
    units = db.Column(db.String(5))
    type = db.Column(db.String(10), nullable=False)
    enabled = db.Column(db.String(20), nullable=False)

    def __init__(self, sensor_name, metric, threshold, operator, units, type, enabled):

        self.sensor_name = sensor_name
        self.metric = metric
        self.threshold = threshold
        self.operator = operator
        self.units = units
        self.type = type
        self.enabled = enabled
