from web.database import db
import logging
from web.functions import get_now
from web.mail import MailClient
from web.sensors.models import Alerts
import config
from jinja2.loaders import PackageLoader
from jinja2 import Environment


LOGGER = logging.getLogger(__name__)


class CheckAlert(object):

    def __init__(self, metric, sensor, threshold, reading, operator, check_id, observers):
        self._metric = metric
        self._sensor = sensor
        self._threshold = threshold
        self._reading = reading
        self._operator = operator
        self._check_id = check_id
        self._observers = observers

    def mail(self, mail, user):
        subject = "Sensor Alert - {} - {}".format(self._sensor.name, self._metric)
        j2_env = Environment(loader=PackageLoader(__name__), trim_blocks=True)
        html_body = j2_env.get_template('error_mail.html').render(
            timestamp=get_now(),
            alert='{} {} is {}. Threshold: {}'.format(self._sensor.name, self._metric, self._reading, self._threshold)
        )
        mail.send(user.email, subject, html_body)

    def sms(self, user):
        raise NotImplemented

    def pushbullet(self, user):
        raise NotImplemented

    def alert(self):
        mail = MailClient(config.MAIL_SERVER, config.MAIL_PORT, config.MAIL_USERNAME, config.MAIL_PASSWORD)
        for user in self._observers:
            if user.email_alert is True:
                self.mail(mail, user)
            if user.sms_alert is True:
                self.sms(user)
            if user.pushbullet_alert is True:
                self.pushbullet(user)

    def execute(self):
        expression = "{0} {1} {2}".format(self._reading, self._operator, self._threshold)
        if eval(expression) is True:
            alert = Alerts.query.filter(Alerts.check_id == self._check_id).filter(Alerts.metric == self._metric). \
                filter(Alerts.status == 'open').first()
            if alert:
                LOGGER.info('Alert for {} {} already exists. Updating.'.format(self._sensor.name, self._metric,
                                                                               self._reading))
                alert.reading = self._reading
                alert.last_check = get_now()
                db.session.commit()
            else:
                LOGGER.info('Creating alert for {} {} is {}'.format(self._sensor.name, self._metric, self._reading))
                alert = Alerts(self._sensor.id, self._metric, self._threshold, self._reading, self._operator, 'open',
                               self._check_id)
                db.session.add(alert)
                db.session.commit()
                self.alert()
        else:
            LOGGER.info('No action needed for {} {}'.format(self._sensor.name, self._metric))
