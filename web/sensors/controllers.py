import pytz
from flask import redirect, render_template, url_for, current_app, request, flash, jsonify
from web.functions import get_now
from web.database import db
from flask import Blueprint
from web.sensors.forms import CreateMijiaSensor, CreateFloraSensor
from web.sensors.models import Mijia, TempHumRead, Alerts, Checks, Flora, FloraRead
from flask_login import login_required, current_user
import datetime
from btlewrap import BluepyBackend

mod_sensors = Blueprint('sensors', __name__, url_prefix='/sensors')


@mod_sensors.route("/alerts", methods=['GET'])
@login_required
def alerts():
    return render_template('sensors/alerts.html', current_app=current_app, alerts=Alerts.query.all())


@mod_sensors.route("/checks", methods=['GET'])
@login_required
def checks():
    return render_template('sensors/checks.html', current_app=current_app, checks=Checks.query.all())


@mod_sensors.route("/alerts/close/<alert_id>", methods=['POST'])
@login_required
def close_alert(alert_id):
    alert = Alerts.query.filter(Alerts.id == alert_id).first()
    alert.status = 'closed'
    alert.closed_on = get_now()
    alert.closed_by = current_user.username
    db.session.commit()
    return redirect(url_for("core.dashboard"))


@mod_sensors.route("/mijia", methods=['GET'])
@login_required
def mijia():
    return render_template('sensors/mijia.html',
                           current_app=current_app,
                           sensors=Mijia.query.all()
                           )


@mod_sensors.route("/mijia/create", methods=['POST', 'GET'])
@login_required
def mijia_create():
    form = CreateMijiaSensor()
    if form.validate_on_submit():
        sensor = Mijia(form.name.data, form.address.data)
        db.session.add(sensor)

        if form.temperature_status.data == 'true':
            temperature_check = Checks(form.name.data, 'temperature', form.temperature_threshold.data,
                                       form.temperature_operator.data, 'C', 'default', 'true')
            db.session.add(temperature_check)

        if form.humidity_status.data == 'true':
            humidity_check = Checks(form.name.data, 'humidity', form.humidity_threshold.data,
                                    form.humidity_operator.data, '%', 'default', 'true')
            db.session.add(humidity_check)

        if form.battery_status.data == 'true':
            battery_check = Checks(form.name.data, 'battery', form.battery_threshold.data,
                                   form.battery_operator.data, '%', 'default', 'true')
            db.session.add(battery_check)

        db.session.commit()

        return redirect(url_for("application.settings"))
    else:
        for field, errors in form.errors.items():
            for error in errors:
                flash(u"Error in the {} field - {}".format(getattr(form, field).label.text, error), 'warning')

    return redirect(url_for("application.settings"))


@mod_sensors.route("/mijia/delete/<sensor_id>", methods=['POST'])
@login_required
def mijia_delete(sensor_id):
    sensor = Mijia.query.filter(Mijia.id == sensor_id).first()
    Checks.query.filter(Checks.sensor_name == sensor.name).delete()
    Mijia.query.filter(Mijia.id == sensor_id).delete()
    db.session.commit()

    return jsonify({"sensor_id": sensor_id, "status": "deleted"})


@mod_sensors.route("/statistics/<sensor_type>", methods=['GET'])
@login_required
def statistics(sensor_type):
    if sensor_type == 'mijia':
        sensors = Mijia.query.all()
    elif sensor_type == 'flora':
        sensors = Flora.query.all()
    else:
        sensors = []

    if request.args.get('sensor') and request.args.get('dates') and request.args.get('timezone'):
        sensor = request.args.get("sensor")
        start_date, end_date = request.args.get("dates").split(' - ')
        start_date = start_date.strip()
        end_date = end_date.strip()

        start_date = pytz.timezone(request.args.get('timezone')).\
            localize(datetime.datetime.strptime(start_date, "%Y-%m-%d %H:%M:%S"), is_dst=None).\
            astimezone(pytz.utc)
        end_date = pytz.timezone(request.args.get('timezone')).\
            localize(datetime.datetime.strptime(end_date, "%Y-%m-%d %H:%M:%S"), is_dst=None).\
            astimezone(pytz.utc)

        if sensor_type == 'mijia':
            result = TempHumRead.query.filter(TempHumRead.sensor_id == sensor). \
                filter(TempHumRead.timestamp.between(start_date, end_date)).all()
        elif sensor_type == 'flora':
            result = FloraRead.query.filter(FloraRead.sensor_id == sensor). \
                filter(FloraRead.timestamp.between(start_date, end_date)).all()
        else:
            result = []

        return render_template('sensors/statistics.html', current_app=current_app, result=result, sensor=sensors,
                               sensor_type=sensor_type, dates=request.args.get("dates"), sensors=sensors)
    else:
        return render_template('sensors/statistics.html', current_app=current_app, sensor_type=sensor_type,
                               sensors=sensors)


@mod_sensors.route("/flora", methods=['GET'])
@login_required
def flora():
    return render_template('sensors/flora.html', current_app=current_app, sensors=Flora.query.all())


@mod_sensors.route("/flora/create", methods=['POST'])
@login_required
def flora_create():
    form = CreateFloraSensor()
    if form.validate_on_submit():
        sensor = Flora(form.name.data, form.address.data)
        db.session.add(sensor)

        if form.temperature_status.data == 'true':
            temperature_check = Checks(form.name.data, 'temperature', form.temperature_threshold.data,
                                       form.temperature_operator.data, 'C', 'default', 'true')
            db.session.add(temperature_check)

        if form.moisture_status.data == 'true':
            moisture_check = Checks(form.name.data, 'moisture', form.moisture_threshold.data,
                                    form.moisture_operator.data, '%', 'default', 'true')
            db.session.add(moisture_check)

        if form.light_status.data == 'true':
            light_check = Checks(form.name.data, 'light', form.light_threshold.data, form.light_operator.data, 'Lux',
                                 'default', 'true')
            db.session.add(light_check)

        if form.conductivity_status.data == 'true':
            conductivity_check = Checks(form.name.data, 'conductivity', form.conductivity_threshold.data,
                                        form.conductivity_operator.data, 'µS/cm', 'default', 'true')
            db.session.add(conductivity_check)

        if form.battery_status.data == 'true':
            battery_check = Checks(form.name.data, 'battery', form.battery_threshold.data,
                                   form.battery_operator.data, '%', 'default', 'true')
            db.session.add(battery_check)

        db.session.commit()

    else:
        for field, errors in form.errors.items():
            for error in errors:
                flash(u"Error in the {} field - {}".format(getattr(form, field).label.text, error), 'warning')

    return redirect(url_for("application.settings"))


@mod_sensors.route("/flora/delete/<sensor_id>", methods=['POST'])
@login_required
def flora_delete(sensor_id):
    sensor = Flora.query.filter(Flora.id == sensor_id).first()
    Checks.query.filter(Checks.sensor_name == sensor.name).delete()
    Flora.query.filter(Flora.id == sensor_id).delete()
    db.session.commit()

    return jsonify({"sensor_id": sensor_id, "status": "deleted"})


@mod_sensors.route("/scan", methods=['GET'])
@login_required
def scan():
    devices = list()
    for device in BluepyBackend.scan_for_devices(10):
        devices.append(device)
    return jsonify(devices)
