from flask_sqlalchemy import SQLAlchemy
from web.functions import database_link
import config

database_link = database_link(config.DATABASE_TYPE, config.DATABASE_NAME, config.DATABASE_HOST, config.DATABASE_USER,
                              config.DATABASE_PASSWORD, config.DATABASE_PORT)

db = SQLAlchemy()

