#!/usr/bin/python3
import datetime
from web.functions import authenticate_scheduler_api, failed_job_listener
from web.security import login_manager
from apscheduler.events import EVENT_JOB_EXECUTED, EVENT_JOB_ERROR
from flask_principal import Principal, identity_loaded, RoleNeed, UserNeed
from web.application.controllers import mod_application
from web.cameras.controllers import mod_cameras
from web.functions import sigterm_handler, database_link, failed_job_listener, authenticate_scheduler_api
from flask import Flask, redirect, render_template, url_for, current_app, flash
from flask_wtf.csrf import CSRFProtect
from logging.handlers import RotatingFileHandler
from web.manager.api import ListUsersAPI, CreateUserAPI, UserAPI, SettingsAPI
from web.relays.controllers import mod_relays
from web.sensors.controllers import mod_sensors
from web.tasks.controllers import mod_tasks
from web.version import __version__ as version
from web.scheduler import JOBS, scheduler
from flask_login import LoginManager, current_user, login_required
from web.manager.controllers import mod_manager, User
from web.auth.controllers import mod_auth
from web.database import db, database_link
from web.mail import mail
from web.roles import admin_permission
from flask_moment import Moment
from flask_restful import Api
from flask_apscheduler.auth import HTTPBasicAuth
import logging
import yaml
import os

app = Flask(__name__)
app.config.from_object('config')
app.config['VERSION'] = version
app.config['REMEMBER_COOKIE_DURATION'] = datetime.timedelta(hours=12)
app.config['SESSION_COOKIE_HTTPONLY'] = True
app.config['REMEMBER_COOKIE_HTTPONLY'] = True
app.config['SQLALCHEMY_DATABASE_URI'] = database_link
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SQLALCHEMY_ECHO'] = False
app.config['JOBS'] = JOBS

if app.config['LOG_ENABLE'] is True:
    file_handler = RotatingFileHandler(app.config['LOG'], maxBytes=app.config['LOG_MAX_SIZE_BYTES'], backupCount=3)
    formatter = logging.Formatter('[%(levelname)s] [%(asctime)-15s] [PID: %(process)d] [%(name)s] %(message)s')
    file_handler.setFormatter(formatter)
    app.logger.addHandler(file_handler)
    app.logger.setLevel(logging.DEBUG)
    app.logger.info('Starting application')

csrf = CSRFProtect(app)
Principal(app)
login_manager.init_app(app)
mail.init_app(app)
db.app = app
db.init_app(app)
moment = Moment(app)

app.register_blueprint(mod_manager)
app.register_blueprint(mod_auth)
app.register_blueprint(mod_application)
app.register_blueprint(mod_tasks)
app.register_blueprint(mod_sensors)
app.register_blueprint(mod_cameras)
app.register_blueprint(mod_relays)

api = Api(app)
api.add_resource(ListUsersAPI, '/api/v1/users/list')
api.add_resource(CreateUserAPI, '/api/v1/user/create')
api.add_resource(UserAPI, '/api/v1/user/<string:user_id>')
api.add_resource(SettingsAPI, '/api/v1/settings')

scheduler.api_enabled = True
scheduler.init_app(app)
scheduler.start()
scheduler.auth = HTTPBasicAuth()
scheduler.authenticate(authenticate_scheduler_api)
scheduler.add_listener(failed_job_listener, EVENT_JOB_EXECUTED | EVENT_JOB_ERROR)
scheduler.add_job('0', 'web.relays.tasks:boot', name='boot')

if app.config['LDAP']:
    from flask_ldap3_login import LDAP3LoginManager
    login_manager = LoginManager(app)  # Setup a Flask-Login Manager
    ldap_manager = LDAP3LoginManager(app)  # Setup a LDAP3 Login Manager.

    @ldap_manager.save_user
    def save_user(dn, username, data, memberships):
        user = User.query.filter(User.username == username).first()
        if user:
            user.first_name = data[app.config['LDAP_USER_FIRST_NAME_ATTR']][0]
            user.last_name = data[app.config['LDAP_USER_LAST_NAME_ATTR']][0]
            user.email = data[app.config['LDAP_USER_EMAIL_ATTR']]
            user.phone = data[app.config['LDAP_USER_MOBILE_ATTR']][0]
            user.role = 'user'
            for user_role in data[app.config['LDAP_USER_MEMBERSHIP_ATTR']]:
                if user_role == app.config['ADMIN_GROUP']:
                    user.role = user_role
            db.session.commit()

        else:
            role = 'user'
            for user_role in data[app.config['LDAP_USER_MEMBERSHIP_ATTR']]:
                if user_role == app.config['ADMIN_GROUP']:
                    role = user_role
            user = User(
                data[app.config['LDAP_USER_FIRST_NAME_ATTR']][0],
                data[app.config['LDAP_USER_LAST_NAME_ATTR']][0],
                username,
                None,
                data[app.config['LDAP_USER_EMAIL_ATTR']],
                role,
                data[app.config['LDAP_USER_MOBILE_ATTR']][0],
                'ldap'
            )
            db.session.add(user)
            db.session.commit()

        return user

# @identity_loaded.connect_via(app)
# def on_identity_loaded(sender, identity):
#     # Set the identity user object
#     identity.user = current_user
#     if hasattr(current_user, 'id'):
#         identity.provides.add(UserNeed(current_user.id))
#     if hasattr(current_user, 'role'):
#         for role in current_user.role:
#             identity.provides.add(RoleNeed(role))


@identity_loaded.connect_via(app)
def on_identity_loaded(sender, identity):
    # Set the identity user object
    identity.user = current_user
    if hasattr(current_user, 'id'):
        identity.provides.add(UserNeed(current_user.id))
    if hasattr(current_user, 'role'):
        identity.provides.add(RoleNeed(current_user.role))


@login_manager.user_loader
def user_loader(user_id):
    return User.query.get(user_id)


@login_manager.unauthorized_handler
def unauthorized_handler():
    flash('Please log in to access this page', 'info')
    return redirect(url_for("index"))


@app.errorhandler(403)
@login_required
def no_rights(error):
    return render_template('403.html', error=error, current_app=current_app), 403


@app.errorhandler(404)
@login_required
def not_found(error):
    return render_template('404.html', error=error, current_app=current_app), 404


@app.errorhandler(500)
@login_required
def not_found(error):
    db.session.rollback()
    return render_template('500.html', error=error, current_app=current_app), 500


@app.before_first_request
def create_admin():
    # Database creation is done on the boot sequence task
    # db.create_all()
    if not User.query.filter(User.username == 'admin').first():
        user = User('Xiaopi',
                    'admin',
                    'admin',
                    'pbkdf2:sha256:50000$QZildwvb$ec2954dfe34d5a540d1aa9b64ce8628ab34b4f8d64a04208f15082a431bc5631',
                    'jorge.nsoares@gmail.com',
                    app.config['ADMIN_GROUP'])
        db.session.add(user)
        db.session.commit()


@app.route("/")
def index():
    if current_user.is_authenticated:
        return redirect(url_for("application.dashboard"))
    else:
        return render_template("index.html", current_app=current_app)
