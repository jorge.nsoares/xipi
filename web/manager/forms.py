from flask_wtf import FlaskForm, validators
from wtforms import StringField, PasswordField, validators, BooleanField
from wtforms.validators import DataRequired
from web.manager.models import User


class CreateUserForm(FlaskForm):
    first_name = StringField('first_name', validators=[DataRequired()])
    last_name = StringField('last_name', validators=[DataRequired()])
    username = StringField('username', validators=[DataRequired()])
    email = StringField('email', [
        validators.DataRequired(),
        validators.Email("Please enter your email address.")
    ])
    password = PasswordField('new_password', [
        validators.DataRequired(),
        validators.EqualTo('verify_password', message='Passwords must match')
    ])
    verify_password = PasswordField('verify_password')
    role = StringField('role')

    def __init__(self, *args, **kwargs):
        FlaskForm.__init__(self, *args, **kwargs)
        self.user = None

    def validate(self):
        rv = FlaskForm.validate(self)
        if not rv:
            return False

        user = User.query.filter_by(username=self.username.data).first()
        if user:
            self.username.errors.append('User already exists')
            return False

        return True


class UpdateProfileForm(FlaskForm):
    first_name = StringField('first_name', validators=[DataRequired()])
    email = StringField('email', [
        validators.DataRequired(),
        validators.Email("Please enter your email address.")
    ])
    last_name = StringField('last_name', validators=[DataRequired()])
    email_alerts = BooleanField('email_alerts')
    sms_alerts = BooleanField('sms_alerts')
    pushbullet_alert = BooleanField('pushbullet_alert')
    phone = StringField('phone')
    pushbullet_key = StringField('pushbullet_key')


class UpdateSettingsForm(FlaskForm):
    application_name = StringField('application_name', validators=[DataRequired()])
    server_ip = StringField('server_ip', validators=[DataRequired()])
    log = StringField('log', validators=[DataRequired()])
    upload_folder = StringField('upload_folder', validators=[DataRequired()])
    recaptcha_public_key = PasswordField('recaptcha_public_key')
    recaptcha_private_key = PasswordField('recaptcha_private_key')
    admins_group = StringField('admins_group', validators=[DataRequired()])
    secret_key = PasswordField('secret_key')
    debug = StringField('debug')
    csrf_enabled = StringField('csrf_enabled')
    recaptcha = StringField('recaptcha')
    db_server = StringField('db_server')
    db_port = StringField('db_port')
    db_username = StringField('db_username')
    db_password = PasswordField('db_password')
    db_name = StringField('db_name', validators=[DataRequired()])
    db_type = StringField('db_type')
    mail_server = StringField('mail_server')
    mail_port = StringField('mail_port')
    mail_username = StringField('mail_username')
    mail_default_sender = StringField('mail_default_sender')
    mail_password = PasswordField('mail_password')
    mail_use_ssl = StringField('mail_use_ssl')
    mail_use_tls = StringField('mail_use_tls')
    ldap_server = StringField('ldap_server')
    ldap_base_dn = StringField('ldap_base_dn')
    ldap_user_dn = StringField('ldap_user_dn')
    ldap_bind_user = StringField('ldap_bind_user')
    ldap_bind_user_password = PasswordField('ldap_bind_user_password')
    ldap_port = StringField('ldap_port')
    ldap_user_rdn = StringField('ldap_user_rdn')
    ldap_user_login_attr = StringField('ldap_user_login_attr')
    ldap_user_first_name = StringField('ldap_user_first_name')
    ldap_user_last_name = StringField('ldap_user_last_name')
    ldap_user_email = StringField('ldap_user_email')
    ldap_user_mobile = StringField('ldap_user_mobile')
    ldap_user_membership = StringField('ldap_user_membership')
    ldap_pwm_url = StringField('ldap_pwm_url')
    skin_color = StringField('skin_color')
