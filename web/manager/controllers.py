from web.security import login_manager
from web.manager.forms import CreateUserForm, UpdateProfileForm, UpdateSettingsForm
from flask import request, redirect, render_template, flash, url_for, current_app, session
from werkzeug.security import generate_password_hash
from web.functions import allowed_file
from web.manager.models import User
from web.database import db
from web.roles import admin_permission
from flask import Blueprint
from flask_login import login_required, current_user, fresh_login_required
from web.sensors.models import Mijia
import configparser
import os


mod_manager = Blueprint('manager', __name__, url_prefix='/manager')


@mod_manager.route("/profile", methods=['GET', 'POST'])
@fresh_login_required
def profile():
    form = UpdateProfileForm()
    if form.validate_on_submit():
        user = User.query.filter(User.id == current_user.id).first_or_404()
        user.first_name = form.first_name.data
        user.last_name = form.last_name.data
        user.email = form.email.data
        user.email_alert = form.email_alerts.data
        user.pushbullet_alert = form.pushbullet_alert.data
        user.sms_alert = form.sms_alerts.data
        user.phone = form.phone.data
        user.pushbullet_key = form.pushbullet_key.data
        db.session.commit()
        flash('Profile Updated successfully', 'success')

    return render_template('manager/profile.html', current_app=current_app, form=form)


@mod_manager.route('/profile/picture', methods=['POST'])
@login_required
def upload_file():
    # check if the post request has the file part
    if 'file' not in request.files:
        flash('No file part', 'warning')
        return redirect(url_for("manager.profile"))

    file = request.files['file']
    if file.filename == '':
        flash('No selected file', 'warning')
        return redirect(url_for("manager.profile"))
    if file and allowed_file(file.filename):
        file.save(os.path.join(current_app.config['UPLOAD_FOLDER'], current_user.username + '.png'))

    return redirect(url_for("manager.profile"))


@mod_manager.route("/user/<action>/<user_id>", methods=['GET', 'POST'])
@admin_permission.require(http_exception=403)
@fresh_login_required
def edit_user(action, user_id):
    form = CreateUserForm()

    if action == 'create' and form.validate_on_submit():
        role = form.role.data
        if role == '':
            role = 'user'

        hashed_password = generate_password_hash(form.password.data)
        user = User(form.first_name.data, form.last_name.data, form.username.data, hashed_password, form.email.data,
                    role)
        db.session.add(user)
        db.session.commit()
        return redirect(url_for("manager.users"))

    elif request.method == 'POST' and action == 'delete' and user_id:
        User.query.filter(User.id == user_id).delete()
        db.session.commit()
        return url_for("manager.users")
    else:
        for field, errors in form.errors.items():
            for error in errors:
                flash(error, 'warning')

    return render_template('manager/user_create.html', current_app=current_app, form=form)


@mod_manager.route("/users", methods=['GET'])
@admin_permission.require(http_exception=403)
@login_required
def users():
    return render_template('manager/users.html', users=User.query.order_by(User.id.asc()).all(), current_app=current_app)


@mod_manager.route("/logs", methods=['GET'])
@login_required
def logs():
    with open(current_app.config['LOG'], "r") as f:
        app_log = f.read()

    return render_template('manager/logs.html', app_log=app_log, current_app=current_app)


@mod_manager.route("/sidebar-toggle", methods=['POST'])
@login_required
def sidebar_toggle():
    if session['sidebar'] == 'sidebar-collapse':
        session['sidebar'] = 'sidebar-open'
    else:
        session['sidebar'] = 'sidebar-collapse'

    return 'sidebar toggled'


