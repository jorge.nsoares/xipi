from web.database import db
from web.functions import get_now
import uuid


class Schedules(db.Model):
    __tablename__ = 'relays.schedules'

    id = db.Column(db.Integer, primary_key=True, index=True)
    uuid = db.Column(db.String(255), nullable=False, unique=True, index=True)
    relay = db.Column(db.String(100), nullable=False, index=True)
    start_time = db.Column(db.DateTime, nullable=False)
    stop_time = db.Column(db.DateTime, nullable=False)
    enabled = db.Column(db.String(10), nullable=False, index=True)
    created_on = db.Column(db.DateTime, nullable=False)
    created_by = db.Column(db.String(80), index=True)

    def __init__(self, relay, start_time, stop_time, enabled, created_by):
        self.uuid = str(uuid.uuid4())
        self.relay = relay
        self.start_time = start_time
        self.stop_time = stop_time
        self.enabled = enabled
        self.created_on = get_now()
        self.created_by = created_by


class Log(db.Model):
    __tablename__ = 'relays.log'

    id = db.Column(db.Integer, primary_key=True, index=True)
    timestamp = db.Column(db.DateTime, nullable=False, index=True)
    relay = db.Column(db.String(100), nullable=False, index=True)
    status = db.Column(db.String(5), nullable=False)
    pin = db.Column(db.Integer, nullable=False)
    source = db.Column(db.String(80), index=True)

    def __init__(self, relay, status, pin, source):
        self.timestamp = get_now()
        self.relay = relay
        self.status = status
        self.pin = pin
        self.source = source


class Status(db.Model):
    __tablename__ = 'relays.status'

    id = db.Column(db.Integer, primary_key=True)
    pin = db.Column(db.Integer,  nullable=False, index=True)
    status = db.Column(db.Boolean, nullable=False, index=True)

    def __init__(self, pin, status):
        self.pin = pin
        self.status = status
