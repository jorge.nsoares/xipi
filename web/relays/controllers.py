import pytz
from flask import Blueprint, render_template, request, current_app, flash, redirect, url_for, session
from flask_login import login_required, current_user
from web.database import db
from web.functions import get_relays
from web.relays.forms import CreateSchedule
from web.relays.models import Schedules, Log
from web.roles import admin_permission
from dateutil import parser
from web.scheduler import scheduler
from datetime import datetime, time
import datetime

mod_relays = Blueprint('relays', __name__, url_prefix='/relays')
relays = get_relays()


@mod_relays.route("/pirelays", methods=['POST', 'GET'])
@login_required
def pi_relays():
    return render_template('relays/pirelays.html', relays=get_relays()['pirelays'], current_app=current_app,
                           log=Log.query.all())


@mod_relays.route("/schedule/<action>/<schedule_id>", methods=['POST', 'GET'])
@login_required
def add_new_schedule(action, schedule_id):
    clients = relays['pirelays']
    form = CreateSchedule()

    if action == 'add' and form.validate_on_submit():
        relay = form.relay.data
        start_time = form.start_time.data
        stop_time = form.stop_time.data
        for client in clients:
            for item in client.keys():
                if item == relay:
                    pin = client[item]['pin']

        today = datetime.datetime.now().strftime('%Y-%m-%d')
        start_time = '{} {}'.format(today, start_time)
        stop_time = '{} {}'.format(today, stop_time)
        start_time = pytz.timezone(form.timezone.data).\
            localize(datetime.datetime.strptime(start_time, "%Y-%m-%d %H:%M"), is_dst=None).\
            astimezone(pytz.utc)
        stop_time = pytz.timezone(form.timezone.data).\
            localize(datetime.datetime.strptime(stop_time, "%Y-%m-%d %H:%M"), is_dst=None).\
            astimezone(pytz.utc)
        schedule = Schedules(relay, start_time, stop_time, 'true', current_user.username)
        db.session.add(schedule)
        db.session.commit()

        start_hour, start_minute = start_time.strftime('%H:%M').split(':')
        stop_hour, stop_minute = stop_time.strftime('%H:%M').split(':')
        scheduler.add_job('{}-1'.format(schedule.uuid), 'web.relays.tasks:start_relay',
                          args=[relay, pin, current_user.username], name='{} On'.format(relay), timezone='utc',
                          trigger='cron', hour=start_hour, minute=start_minute)

        scheduler.add_job('{}-2'.format(schedule.uuid), 'web.relays.tasks:stop_relay',
                          args=[relay, pin, current_user.username], name='{} Off'.format(relay), timezone='utc',
                          trigger='cron', hour=stop_hour, minute=stop_minute)

        flash('Schedule created successfully!', 'success')

    elif request.method == 'POST' and action == 'delete':
        schedule = Schedules.query.filter(Schedules.id == schedule_id).first_or_404()
        scheduler.delete_job('{}-1'.format(schedule.uuid))
        scheduler.delete_job('{}-2'.format(schedule.uuid))
        Schedules.query.filter(Schedules.id == schedule_id).delete()
        db.session.commit()

    elif request.method == 'POST' and action == 'switch':
        schedule = Schedules.query.filter(Schedules.id == schedule_id).first()
        if schedule.enabled == 'true':
            schedule.enabled = 'false'
            scheduler.pause_job('{}-1'.format(schedule.uuid))
            scheduler.pause_job('{}-2'.format(schedule.uuid))
            db.session.commit()
        else:
            pass

        return url_for('application.dashboard')

    elif action == 'edit' and form.validate_on_submit():
        schedule = Schedules.query.filter(Schedules.id == schedule_id).first_or_404()
        relay = form.relay.data
        start_time = form.start_time.data
        stop_time = form.stop_time.data
        for client in clients:
            for item in client.keys():
                if item == relay:
                    pin = client[item]['pin']

        today = datetime.datetime.now().strftime('%Y-%m-%d')
        start_time = '{} {}'.format(today, start_time)
        stop_time = '{} {}'.format(today, stop_time)
        start_time = pytz.timezone(form.timezone.data).\
            localize(datetime.datetime.strptime(start_time, "%Y-%m-%d %H:%M"), is_dst=None).\
            astimezone(pytz.utc)
        stop_time = pytz.timezone(form.timezone.data).\
            localize(datetime.datetime.strptime(stop_time, "%Y-%m-%d %H:%M"), is_dst=None).\
            astimezone(pytz.utc)

        schedule.relay = relay
        schedule.start_time = start_time
        schedule.stop_time = stop_time
        db.session.commit()

        scheduler.delete_job('{}-1'.format(schedule.uuid))
        scheduler.delete_job('{}-2'.format(schedule.uuid))

        start_hour, start_minute = start_time.strftime('%H:%M').split(':')
        stop_hour, stop_minute = stop_time.strftime('%H:%M').split(':')
        scheduler.add_job('{}-1'.format(schedule.uuid), 'web.relays.tasks:start_relay',
                          args=[relay, pin, current_user.username], name='{} On'.format(relay), timezone='utc',
                          trigger='cron', hour=start_hour, minute=start_minute)

        scheduler.add_job('{}-2'.format(schedule.uuid), 'web.relays.tasks:stop_relay',
                          args=[relay, pin, current_user.username], name='{} Off'.format(relay), timezone='utc',
                          trigger='cron', hour=stop_hour, minute=stop_minute)

        flash('Schedule updated successfully!', 'success')

    elif request.method == 'GET' and action == 'edit':
        schedule = Schedules.query.filter(Schedules.id == schedule_id).first_or_404()
        start_time = pytz.timezone('utc').localize(schedule.start_time, is_dst=None).\
            astimezone(pytz.timezone(session['timezone'])).strftime('%H:%M')
        stop_time = pytz.timezone('utc').localize(schedule.stop_time, is_dst=None).\
            astimezone(pytz.timezone(session['timezone'])).strftime('%H:%M')

        return render_template('relays/schedules_edit.html', clients=clients, current_app=current_app,
                               schedule=schedule, start_time=start_time, stop_time=stop_time)

    else:
        for field, errors in form.errors.items():
            for error in errors:
                flash(error, 'warning')

    return render_template('relays/schedules.html', clients=clients, current_app=current_app,
                               schedule=Schedules.query.all())