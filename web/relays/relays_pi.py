from web.relays.models import Status, Log
from web.database import db


class Relays(object):
    def __init__(self, pin):
        self.pin = pin

    def start(self, relay, source):
        log = Log(relay, 1, self.pin, source)
        db.session.add(log)
        pin_status = Status.query.filter(Status.pin == self.pin).first()
        if pin_status:
            pin_status.status = 1
        else:
            pin_status = Status(self.pin, 1)
            db.session.add(pin_status)
        db.session.commit()

        return 'started {} pin {}'.format(relay, self.pin)

    def stop(self, relay, source):
        log = Log(relay, 0, self.pin, source)
        db.session.add(log)
        pin_status = Status.query.filter(Status.pin == self.pin).first()
        if pin_status:
            pin_status.status = 0
        else:
            pin_status = Status(self.pin, 0)
            db.session.add(pin_status)
        db.session.commit()

        return 'stopped {} pin {}'.format(relay, self.pin)

    def set_mode(self, mode='out'):
        print(mode)
        print(self.pin)
        return mode

    def status(self):
        print(self.pin)
        return self.pin

