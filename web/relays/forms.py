from flask_wtf import FlaskForm, validators
from wtforms import StringField, PasswordField, validators
from wtforms.validators import DataRequired


class CreateSchedule(FlaskForm):
    relay = StringField('relay', validators=[DataRequired()])
    start_time = StringField('start_time', validators=[DataRequired()])
    stop_time = StringField('stop_time', validators=[DataRequired()])
    timezone = StringField('timezone', validators=[DataRequired()])

    def __init__(self, *args, **kwargs):
        FlaskForm.__init__(self, *args, **kwargs)
        self.user = None

    def validate(self):
        rv = FlaskForm.validate(self)
        if not rv:
            return False

        if self.start_time.data >= self.stop_time.data:
            self.stop_time.errors.append('The stop time cannot be >= than the start time, please try again!')
            return False

        return True
