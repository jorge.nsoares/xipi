from web import scheduler
from web.functions import get_relays
from web.relays.models import Schedules, Status
from web.relays.relays_pi import Relays
from web.database import db


def start_relay(relay, pin, source):
    with db.app.app_context():
        Relays(pin).start(relay,source)


def stop_relay(relay, pin, source):
    with db.app.app_context():
        Relays(pin).start(relay, source)


def boot():
    with db.app.app_context():
        db.create_all()
        schedules = Schedules.query.filter(Schedules.enabled == 'true')
        relays = get_relays()['pirelays']
        pin_status = Status.query.all()
        for status in pin_status:
            for item in relays:
                for relay in item.keys():
                    if item[relay]['pin'] == status.pin:
                        print(item[relay]['pin'])
                        print(relay)
                        Relays(status.pin).start(relay, 'boot')

        for schedule in schedules:
            for item in relays:
                for relay in item.keys():
                    if relay == schedule.relay:
                        pin = item[relay]['pin']

            start_hour, start_minute = schedule.start_time.strftime('%H:%M').split(':')
            stop_hour, stop_minute = schedule.stop_time.strftime('%H:%M').split(':')
            scheduler.add_job('{}-1'.format(schedule.uuid), 'web.relays.tasks:start_relay',
                              args=[schedule.relay, pin, schedule.created_by], name='{} On'.format(schedule.relay),
                              timezone='utc', trigger='cron', hour=start_hour, minute=start_minute)
            scheduler.add_job('{}-2'.format(schedule.uuid), 'web.relays.tasks:stop_relay',
                              args=[schedule.relay, pin, schedule.created_by], name='{} Off'.format(schedule.relay),
                              timezone='utc', trigger='cron', hour=stop_hour, minute=stop_minute)
