from flask_login import LoginManager

login_manager = LoginManager()
login_manager.session_protection = "strong"
login_manager.refresh_view = "index"

